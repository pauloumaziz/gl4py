#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Functions to pot-process
"""

from code_aster.Commands import CALC_CHAMP,CREA_CHAMP,IMPR_RESU,DEFI_FICHIER
from .med import createResu

def computeNodalStress(resu,groupElement=None,model=None,mater=None):
    """
    Compute the nodal stress from a result aster
    Input:
        -resu: aster result
        -model: aster model
        -mater: aster material
    """
    if not(model and mater):
        resu = CALC_CHAMP(reuse = resu,
                        RESULTAT = resu,
                        MODELE = resu.getModel(),
                        CHAM_MATER = resu.getMaterialOnMesh(),
                        CONTRAINTE = ('SIGM_ELNO'),
                        GROUP_MA = groupElement,
                        INST = 0.
                        )
    else:
        resu = CALC_CHAMP(reuse = resu,
                        RESULTAT = resu,
                        MODELE = model,
                        CHAM_MATER = mater,
                        CONTRAINTE = ('SIGM_ELNO'),
                        GROUP_MA = groupElement,
                        INST = 0.
                        )

def computeNodalStrain(resu,groupElement=None,model=None,mater=None):
    """
    Compute the nodal stress from a result aster
    Input:
        -resu: aster result
        -model: aster model
        -mater: aster material
    """
    if not(model and mater):
        resu = CALC_CHAMP(reuse = resu,
                        RESULTAT = resu,
                        DEFORMATION = ('EPSI_ELNO'),
                        GROUP_MA = groupElement,
                        INST = 0.
                        )
    else:
        resu = CALC_CHAMP(reuse = resu,
                        RESULTAT = resu,
                        MODELE = model,
                        CHAM_MATER = mater,
                        DEFORMATION = ('EPSI_ELNO'),
                        GROUP_MA = groupElement,
                        INST = 0.
                        )

def computeNodalReaction(resu,groupElement=None,model=None,mater=None):
    """
    Compute the nodal stress from a result aster
    Input:
        -resu: aster result
        -model: aster model
        -mater: aster material
    """
    if not(model and mater):
        resu = CALC_CHAMP(reuse = resu,
                        RESULTAT = resu,
                        FORCE = ('REAC_NODA'),
                        GROUP_MA = groupElement,
                        INST = 0.
                        )
    else:
        resu = CALC_CHAMP(reuse = resu,
                        RESULTAT = resu,
                        MODELE = model,
                        CHAM_MATER = mater,
                        FORCE = ('REAC_NODA'),
                        GROUP_MA = groupElement,
                        INST = 0.
                        )

def computeNodalReactionOnElementsFromDisplacement(displacement,model,mater,elements,groupElements):
    """
    Compute the nodal reaction from displacement on a group of elements 
    and extract them on another group of elements.
    Input:
        -displacement: aster displacement
        -model: aster model
        -mater: aster material
        -elements: group of elements where to extract the nodal reactions
        -groupElements: group of elements where to compute the nodal reactions
    """
    resu = createResu(displacement,model,mater)
    computeNodalReaction(resu,groupElements)
    nodalReaction = CREA_CHAMP(OPERATION='EXTR',
                               NOM_CHAM='REAC_NODA',
                               TYPE_CHAM='NOEU_DEPL_R',
                               RESULTAT=resu,
                               INST=0.)
    nodalReactionOnNodes = CREA_CHAMP(OPERATION='ASSE',
                                      TYPE_CHAM='NOEU_DEPL_R',
                                      MODELE=model,
                                      ASSE=_F(CHAM_GD=nodalReaction,
                                              GROUP_MA=elements),
                                      )
    return nodalReactionOnNodes.EXTR_COMP().valeurs

def saveResu2MED(resu,locate_file):
    """
    Export a field in a MED file
    Input:
        -field: field to save
        -model: aster model
        -mater: assigned aster materials
        -locate_file: path wher to save the MED file
        -param: class of parameters
    """
    onlyFields = [nameField for nameField,isField in resu.LIST_CHAMPS().items() if isField]
    DEFI_FICHIER(ACTION='ASSOCIER',UNITE=83,FICHIER=locate_file,TYPE = 'LIBRE')
    IMPR_RESU(UNITE = 83,
              FORMAT = 'MED',
              RESU = _F(RESULTAT = resu,
                        NOM_CHAM = onlyFields,
                        NOM_CHAM_MED = onlyFields
                        )
              )
    DEFI_FICHIER(ACTION='LIBERER',UNITE=83)