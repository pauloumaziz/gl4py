#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Functions to convert to MED format
"""

import code_aster
from code_aster.Commands import CREA_CHAMP,CREA_RESU,CREA_TABLE

#==============================================================================
# Convert the python field into an aster field
#==============================================================================
def convert2aster(field,nodes,dofs,mesh):
    """
    Convert into an aster field
    """
    dim = dofs['dimension']
    ### A modifier pour éviter le if dégueux
    ### Table creation
    table = CREA_TABLE(LISTE = tuple([_F(LISTE_K = nodes,PARA = 'NOEUD')]+
                                [_F(LISTE_R = field[list(range(ii,dim * len(nodes),dim))],PARA = dofs['python'][ii]) for ii in range(dim)]
                                        )
                                )
    ### Creation of the aster field
    field = CREA_CHAMP(TYPE_CHAM = 'NOEU_DEPL_R',					# Type de champ (ici champ par noeud
                                OPERATION = 'EXTR',
                                TABLE = table,
                                MAILLAGE = mesh,					# Maillage associé
                                )
    return field

#==============================================================================
# Create an aster result
#==============================================================================
def createResu(field,model=None,mat=None,char_cine=None):
    """
    Create an aster concept of results from an aster field obtained by a solve.
    Input:
        -field: aster displacement field
        -model: aster model
        -mat: aster assigned material
        -char_cine: dirichlet boundary conditions
    Output:
        -aster result
    """
    if char_cine:
        resu = CREA_RESU(OPERATION = 'AFFE',
                            TYPE_RESU = 'EVOL_ELAS',
                            NOM_CHAM = 'DEPL',
                            EXCIT = _F(CHARGE = char_cine),
                            AFFE = _F(CHAM_GD = field,
                                    MODELE = model,
                                    CHAM_MATER = mat,
                                    INST = 0.,)
                            )
    else:
        resu = CREA_RESU(OPERATION = 'AFFE',
                     TYPE_RESU = 'EVOL_ELAS',
                     NOM_CHAM = 'DEPL',
                     AFFE = _F(CHAM_GD = field,
                               MODELE = model,
                               CHAM_MATER = mat,
                               INST = 0.,)
                    )

    return resu