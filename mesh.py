#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Functions to manage aster meshes
"""
import pdb
import code_aster
from code_aster.Commands import CREA_CHAMP,DEFI_GROUP

def convertGMA2GNO(GMA,mesh,dofs):
    """
    Convert groups of elements in groups of nodes
    Input:
        -GMA: list of groups of elements
        -mesh: aster mesh
        -dofs: list of dofs
    """
    for gma in GMA:
        # nodes = getNodesFromGma(gma,mesh,dofs)
        # pdb.set_trace()
        # mesh.addGroupOfNodesFromNodes(gma,nodes)
        mesh = DEFI_GROUP(reuse=mesh,
                          MAILLAGE=mesh,
                          CREA_GROUP_NO=_F(NOM=gma,
                                           GROUP_MA=gma))

def getAsterMesh(meshPath):
    """
    Return the mesh aster
    Input:
        -meshPath: path to find the mesh file in MED format
    Output:
        -aster mesh
    """
    mesh = code_aster.Mesh()
    mesh.readMedFile(meshPath)
    return mesh

def getDofFromNodes(nodesTot,nodes,nDofsPerNode):
    """
    Find the associated degrees of freedom of nodes inside a numbering
    of a global list of nodes
    Input: 
        -nodesTot: list of total nodes
        -nodes: list of nodes for the dofs
        -nDofsPerNodes: number of dofs per node
    """
    dict_nodes = {node:pos for pos,node in enumerate(nodesTot)}
    pos_in_nodes = [dict_nodes[node] for node in nodes]
    pos_in_dofs = [nDofsPerNode * ii+jj for ii in pos_in_nodes for jj in range(nDofsPerNode)]

    return pos_in_dofs


def getNodesFromGno(gno,mesh,dofs):
    """
    Return the list of nodes which compose the group of nodes gno
    Input:
        -gno: name of the group of nodes
        -mesh: aster mesh
        -dim: dimension of the problem
    Output:
        -python list of string
    """
    ### Solution temporaire en attendant mieux de asterxx
    dim = len(dofs)
    val = tuple([0. for ii in range(dim)])
    champNO = CREA_CHAMP(OPERATION = "AFFE",
                         TYPE_CHAM = 'NOEU_DEPL_R',
                         MAILLAGE = mesh,
                         AFFE = _F(GROUP_NO = gno,
                                   NOM_CMP = dofs,
                                   VALE = val,
                                   )
                         )
    no = ['N{}'.format(ii) for ii in sorted(list(set(list(champNO.EXTR_COMP(topo = 1).noeud))))]
    return no

def getNodesFromGma(gma,mesh,dofs):
    """
    Return the list of nodes which compose the group of nodes gno
    Input:
        -gno: name of the group of nodes
        -mesh: aster mesh
        -dim: dimension of the problem
    Output:
        -python list of string
    """
    ### Solution temporaire en attendant mieux de asterxx
    dim = len(dofs)
    val = tuple([0. for ii in range(dim)])
    champNO = CREA_CHAMP(OPERATION = "AFFE",
                         TYPE_CHAM = 'NOEU_DEPL_R',
                         MAILLAGE = mesh,
                         AFFE = _F(GROUP_MA = gma,
                                   NOM_CMP = dofs,
                                   VALE = val,
                                   )
                         )
    no = ['N{}'.format(ii) for ii in sorted(list(set(list(champNO.EXTR_COMP(topo = 1).noeud))))]
    return no