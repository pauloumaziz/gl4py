#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Functions to initiate multiple instance of code-aster into a mpi process
"""
import os
from shutil import rmtree
from mpi4py import MPI
import code_aster


def initAster(pathDirName):
    """
    Initiate an aster instance
    Input:
        -pathDirName: path for the directory where to lauch the aster instances
    """
    if os.path.isdir(pathDirName+'/'+os.path.split(pathDirName)[-1]+'_{}'.format(MPI.COMM_WORLD.Get_rank())):
        rmtree(pathDirName+'/'+os.path.split(pathDirName)[-1]+'_{}'.format(MPI.COMM_WORLD.Get_rank()))
    os.makedirs(pathDirName+'/'+os.path.split(pathDirName)[-1]+'_{}'.format(MPI.COMM_WORLD.Get_rank()))
    os.chdir(pathDirName+'/'+os.path.split(pathDirName)[-1]+'_{}'.format(MPI.COMM_WORLD.Get_rank()))
    code_aster.init()
