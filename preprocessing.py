#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Functions to manage stiffness
"""
import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as spl

from .boundary import imposedDOFonGNO,computeNeumann
from .mesh import convertGMA2GNO,getDofFromNodes,getNodesFromGma
from .model import createModel
from .materials import assignMaterialDict
from .operators import computeRHSfromAsterDiri,computeStiffness,convertAsterStiffnessToPython,elimOperatorOnNodes,modifSparseMatrixOnNodes

import pdb

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

class structureGL():
    """
    Class to define a structure with attributes containing aster
    models, aster materials, aster numbering, aster stiffness
    """
    def __init__(self,typePatch,nlocal=1):
        """
        Initiate the class
        Input:
            -nlocal: number of local patchs
        """
        if rank == size-1:
            self.neighbor = range(nlocal)
            # self.type = 'global'
        else:
            self.neighbor = [size-1]
            # self.type = 'local'
        self.type = typePatch
        self.nLocal = nlocal
        self.model = None
        self.material = None
        self.dofs = None
        self.listDofs = dict()
        self.listNodes = dict()
        self.dirichlet = None
        self.matrAsse = None
        self.numbering = None
        self.K = None
        self.C = dict()
        self.t = dict()
        self.operators = dict()
        self.__prepareFunctions = {'local':self.prepareLocal,
                                   'aux':self.prepareAux,
                                   'coarse':self.prepareCoarse,
                                   'global':self.prepareGlobal}

    def extractKbb(self,K):
        """
        Extract the diagonal of the stiffness matrix 
        """

    def prepare(self,mesh,modeling,materialDict,solver,dirichlet=None,neumann=None,num=rank,weighting=1.):
        """
        Prepare the class computing all the aster stuff
        Input:
            -mesh: aster mesh
            -modeling: dictionnary containing the physics and modelings and 
            groups of element where to apply it
            -materialDict: dictionnary of aster materials with the keys as 
            groups of elements where to assign th materials
            -solver: aster solver
            -dirichlet : dictionnary to describe dirichlet BC
            -neumann : dictionnary to describe neumann BC
            -num : optional to specifiy which local or which auxiliar to prepare
        """

        
        ### Create the model
        self.model,self.dofs = createModel(mesh,modeling['physics'],modeling['modelings'],modeling['groupsOfElements'])
        ### Assign the material
        self.material = assignMaterialDict(materialDict,mesh)
        ### Dirichlet boundary conditions
        if dirichlet:
            convertGMA2GNO(dirichlet.keys(),mesh,self.dofs['python'])
            self.dirichlet = imposedDOFonGNO(self.model,dirichlet,self.dofs)
        
        ### Elementary matrix and numbering
        self.matrAsse,self.numbering,dofElim = computeStiffness(self.model,self.material,self.dirichlet)
        ### Compute Dirichlet RHS
        sizeMatrAsse = self.matrAsse.EXTR_MATR(sparse=True)[-1]
        if dirichlet:
            vcine = computeRHSfromAsterDiri(self.dirichlet,self.numbering)
        else:
            vcine = np.zeros(sizeMatrAsse)
        if neumann:
            self.neumann,vneumann = computeNeumann(self.model,self.material,self.numbering,neumann,self.dofs)
        else:
            vneumann = np.zeros(sizeMatrAsse)
        # Convert the stiffness into a scipy sparse matrix
        KG = convertAsterStiffnessToPython(self.matrAsse,True)
        # Compute the eliminated part of the stiffness matrix
        if dirichlet:
            KGdiri = elimOperatorOnNodes(KG,dofElim)
            # Factorization of the aster stiffness
            solver.matrixFactorization(self.matrAsse)
        else:
            KGdiri = sp.csr_matrix((sizeMatrAsse,sizeMatrAsse))
        # Convert the stiffness matrix into scipy sparse matrix. 
        # In this matrix the dofs have been eliminated
        KGElim = convertAsterStiffnessToPython(self.matrAsse,True)
        self.__prepareFunctions[self.type](mesh,num)
        KGElimLU = spl.splu(KGElim)
        KGElimNoModif = KGElim.copy()

        ### Compute the local stiffness with blocked interface for preconditioning
        if not(rank==size-1):
            dirichlet = {"I{}".format(rank):{dof:0. for dof in self.dofs['python']}}
            convertGMA2GNO(dirichlet.keys(),mesh,self.dofs['python'])
            dirichletAster = imposedDOFonGNO(self.model,dirichlet,self.dofs)
            matrAsse,numbering,dofElim = computeStiffness(self.model,self.material,dirichletAster)
            solver.matrixFactorization(matrAsse)
            KLblocked = spl.splu(convertAsterStiffnessToPython(matrAsse,True))
        else:
            KLblocked = None
        ### For a mixed approach, need to extract Kbb on the interface
        H = list()
        if rank == size -1:
            Kbb = list()
            ## Get multiplicity in case of intersections of interfaces
            if self.type == 'global':
                typePatch = 'G'
            elif self.type == 'coarse':
                typePatch = 'C'
            if self.type in {'coarse','global'}:
                Nsum = np.zeros((1,len(self.listDofs[typePatch])))
                for ii in range(self.nLocal):
                    Nsum = Nsum + np.array(self.C[ii].sum(0))
                for ii in range(self.nLocal):
                    multiplicity = self.C[ii].dot(Nsum.T)
                    Kbb.append(sp.diags((self.C[ii].dot(KG.dot(self.C[ii].T)).diagonal())).dot(sp.diags((1/multiplicity.T.ravel()).tolist())))
                    KbbLoc = comm.sendrecv(Kbb[ii],dest=ii,sendtag=ii,recvtag=ii)
                    H.append(sp.linalg.inv(sp.linalg.inv(Kbb[ii]) + sp.linalg.inv(KbbLoc))*weighting)
                modifSparseMatrixOnNodes(KGElim,H,[self.listNodes[jj] for jj in range(self.nLocal)],self.listNodes[typePatch])
        else:
            if self.type in {'local'}:
                Kbb = sp.diags((self.C[0].dot(KG.dot(self.C[0].T)).diagonal()))
                KbbGlo = comm.sendrecv(Kbb,dest=size-1,sendtag=rank,recvtag=rank)
                H = [(sp.linalg.inv(sp.linalg.inv(Kbb) + sp.linalg.inv(KbbGlo)))*weighting]
                modifSparseMatrixOnNodes(KGElim,H,self.listNodes[rank],self.listNodes['L'])
            
        self.operators['K'] = KG
        self.operators['H'] = H
        self.operators['Kelim'] = KGElimNoModif
        self.operators['Kdiri'] = KGdiri
        self.operators['KLU'] = KGElimLU
        self.operators['KLUlatin'] = spl.splu(KGElim)
        self.operators['KLblocked'] = KLblocked
        return vcine,vneumann

    def prepareCoarse(self,mesh,num=rank):
        """
        Prepare specificities for the global part
        Input:
            -mesh: aster mesh
        """
        ### Get nodes at the interfaces
        self.listNodes.update({local:getNodesFromGma("I{}".format(local),mesh,self.dofs['python']) for local in range(self.nLocal)})
        self.listNodes.update({'A{}'.format(local):getNodesFromGma("A{}".format(local),mesh,self.dofs['python']) for local in range(self.nLocal)})
        self.listNodes.update({'I':getNodesFromGma(["I{}".format(local) for local in range(self.nLocal)],mesh,self.dofs['python'])})
        # Get the nodes of the whole global
        self.listNodes['G'] = getNodesFromGma("G",mesh,self.dofs["python"])
        self.listNodes['C'] = getNodesFromGma("C",mesh,self.dofs["python"])
        # Convert to the dofs at the interfaces
        self.listDofs.update({local:getDofFromNodes(self.listNodes['C'],self.listNodes[local],len(self.dofs["python"])) for local in range(self.nLocal)})
        self.listDofs['C'] = getDofFromNodes(self.listNodes['C'],self.listNodes['C'],len(self.dofs["python"]))
        self.listDofs['G'] = getDofFromNodes(self.listNodes['G'],self.listNodes['G'],len(self.dofs["python"]))
        self.listDofs['I'] = getDofFromNodes(self.listNodes['C'],self.listNodes['I'],len(self.dofs["python"]))
        self.listDofs.update({'A{}'.format(ii):getDofFromNodes(self.listNodes['G'],self.listNodes['A{}'.format(ii)],len(self.dofs["python"])) for ii in range(self.nLocal)})
        self.C.update({ii:sp.coo_matrix((np.ones(len(self.listDofs[ii])),(range(len(self.listDofs[ii])),self.listDofs[ii])),shape=(len(self.listDofs[ii]),len(self.listNodes['C'])*len(self.dofs['python']))).tocsc() for ii in range(self.nLocal)})
        self.C['I'] = sp.coo_matrix((np.ones(len(self.listDofs['I'])),(range(len(self.listDofs['I'])),self.listDofs['I'])),shape=(len(self.listDofs['I']),len(self.listNodes['C'])*len(self.dofs['python']))).tocsc()
        self.t['C'] = sp.coo_matrix((np.ones(len(self.listDofs['C'])),(range(len(self.listDofs['C'])),self.listDofs['C'])),shape=(len(self.listDofs['C']),len(self.listNodes['C'])*len(self.dofs['python']))).tocsc()
        self.t.update({'A{}'.format(ii):sp.coo_matrix((np.ones(len(self.listDofs['A{}'.format(ii)])),(range(len(self.listDofs['A{}'.format(ii)])),self.listDofs['A{}'.format(ii)])),shape=(len(self.listDofs['A{}'.format(ii)]),len(self.listNodes['C']) * len(self.dofs['python']))).tocsc() for ii in range(self.nLocal)})

    # def prepareCoarse(self,mesh,num=rank):
    #     """
    #     Prepare specificities for the global part
    #     Input:
    #         -mesh: aster mesh
    #     """
    #     ### Get nodes at the interfaces
    #     self.listNodes.update({local:getNodesFromGma("I{}".format(local),mesh,self.dofs['python']) for local in range(self.nLocal)})
    #     self.listNodes.update({'A{}'.format(local):getNodesFromGma("A{}".format(local),mesh,self.dofs['python']) for local in range(self.nLocal)})
    #     self.listNodes.update({'I':getNodesFromGma(["I{}".format(local) for local in range(self.nLocal)],mesh,self.dofs['python'])})
    #     # Get the nodes of the whole global
    #     self.listNodes['G'] = getNodesFromGma("G",mesh,self.dofs["python"])
    #     self.listNodes['C'] = getNodesFromGma("C",mesh,self.dofs["python"])
    #     # Convert to the dofs at the interfaces
    #     self.listDofs.update({local:getDofFromNodes(self.listNodes['C'],self.listNodes[local],len(self.dofs["python"])) for local in range(self.nLocal)})
    #     self.listDofs['C'] = getDofFromNodes(self.listNodes['C'],self.listNodes['C'],len(self.dofs["python"]))
    #     self.listDofs['G'] = getDofFromNodes(self.listNodes['G'],self.listNodes['G'],len(self.dofs["python"]))
    #     self.listDofs['I'] = getDofFromNodes(self.listNodes['G'],self.listNodes['I'],len(self.dofs["python"]))
    #     self.listDofs.update({'A{}'.format(ii):getDofFromNodes(self.listNodes['G'],self.listNodes['A{}'.format(ii)],len(self.dofs["python"])) for ii in range(self.nLocal)})
    #     self.C.update({ii:sp.coo_matrix((np.ones(len(self.listDofs[ii])),(range(len(self.listDofs[ii])),self.listDofs[ii])),shape=(len(self.listDofs[ii]),len(self.listNodes['C'])*len(self.dofs['python']))).tocsc() for ii in range(self.nLocal)})
    #     self.C['I'] = sp.coo_matrix((np.ones(len(self.listDofs['I'])),(range(len(self.listDofs['I'])),self.listDofs['I'])),shape=(len(self.listDofs['I']),len(self.listNodes['G'])*len(self.dofs['python']))).tocsc()
    #     self.t['C'] = sp.coo_matrix((np.ones(len(self.listDofs['C'])),(range(len(self.listDofs['C'])),self.listDofs['C'])),shape=(len(self.listDofs['C']),len(self.listNodes['C'])*len(self.dofs['python']))).tocsc()
    #     self.t.update({'A{}'.format(ii):sp.coo_matrix((np.ones(len(self.listDofs['A{}'.format(ii)])),(range(len(self.listDofs['A{}'.format(ii)])),self.listDofs['A{}'.format(ii)])),shape=(len(self.listDofs['A{}'.format(ii)]),len(self.listNodes['G']) * len(self.dofs['python']))).tocsc() for ii in range(self.nLocal)})

    def prepareGlobal(self,mesh,num=rank):
        """
        Prepare specificities for the global part
        Input:
            -mesh: aster mesh
        """
        ### Get nodes at the interfaces
        self.listNodes.update({local:getNodesFromGma("I{}".format(local),mesh,self.dofs['python']) for local in range(self.nLocal)})
        self.listNodes.update({'A{}'.format(local):getNodesFromGma("A{}".format(local),mesh,self.dofs['python']) for local in range(self.nLocal)})
        self.listNodes.update({'I':getNodesFromGma(["I{}".format(local) for local in range(self.nLocal)],mesh,self.dofs['python'])})
        # Get the nodes of the whole global
        self.listNodes['G'] = getNodesFromGma("G",mesh,self.dofs["python"])
        self.listNodes['C'] = getNodesFromGma("C",mesh,self.dofs["python"])
        # Convert to the dofs at the interfaces
        self.listDofs.update({local:getDofFromNodes(self.listNodes['G'],self.listNodes[local],len(self.dofs["python"])) for local in range(self.nLocal)})
        self.listDofs['C'] = getDofFromNodes(self.listNodes['G'],self.listNodes['C'],len(self.dofs["python"]))
        self.listDofs['G'] = getDofFromNodes(self.listNodes['G'],self.listNodes['G'],len(self.dofs["python"]))
        self.listDofs['I'] = getDofFromNodes(self.listNodes['G'],self.listNodes['I'],len(self.dofs["python"]))
        self.listDofs.update({'A{}'.format(ii):getDofFromNodes(self.listNodes['G'],self.listNodes['A{}'.format(ii)],len(self.dofs["python"])) for ii in range(self.nLocal)})
        self.C.update({ii:sp.coo_matrix((np.ones(len(self.listDofs[ii])),(range(len(self.listDofs[ii])),self.listDofs[ii])),shape=(len(self.listDofs[ii]),len(self.listNodes['G'])*len(self.dofs['python']))).tocsc() for ii in range(self.nLocal)})
        self.C['I'] = sp.coo_matrix((np.ones(len(self.listDofs['I'])),(range(len(self.listDofs['I'])),self.listDofs['I'])),shape=(len(self.listDofs['I']),len(self.listNodes['G'])*len(self.dofs['python']))).tocsc()
        self.t['C'] = sp.coo_matrix((np.ones(len(self.listDofs['C'])),(range(len(self.listDofs['C'])),self.listDofs['C'])),shape=(len(self.listDofs['C']),len(self.listNodes['G'])*len(self.dofs['python']))).tocsc()
        self.t.update({'A{}'.format(ii):sp.coo_matrix((np.ones(len(self.listDofs['A{}'.format(ii)])),(range(len(self.listDofs['A{}'.format(ii)])),self.listDofs['A{}'.format(ii)])),shape=(len(self.listDofs['A{}'.format(ii)]),len(self.listNodes['G']) * len(self.dofs['python']))).tocsc() for ii in range(self.nLocal)})

    def prepareLocal(self,mesh,num=rank):
        """
        Prepare specificities for the local parts
        Input:
            -mesh: aster mesh
        """
        ### Get nodes at the interfaces
        self.listNodes.update({num:getNodesFromGma("I{}".format(num),mesh,self.dofs['python'])})
        # Get the nodes of the local
        self.listNodes['L'] = getNodesFromGma("L{}".format(num),mesh,self.dofs["python"])
        # Convert to the dofs at the interfaces
        self.listDofs['G'] = getDofFromNodes(self.listNodes['L'],self.listNodes['L'],len(self.dofs["python"]))
        self.listDofs.update({0:getDofFromNodes(self.listNodes['L'],self.listNodes[num],len(self.dofs["python"]))})
        self.C.update({0:sp.coo_matrix((np.ones(len(self.listDofs[0])),(range(len(self.listDofs[0])),self.listDofs[0])),shape=(len(self.listDofs[0]),len(self.listNodes['L'])*len(self.dofs['python']))).tocsc()})
        self.t['L'] = sp.coo_matrix((np.ones(len(self.listDofs[0])),(range(len(self.listDofs[0])),self.listDofs[0])),shape=(len(self.listDofs[0]),len(self.listNodes['L'])*len(self.dofs['python']))).tocsc()

    def prepareAux(self,mesh,num=rank):
        """
        Prepare specificities for the local parts
        Input:
            -mesh: aster mesh
        """
        ### Get nodes at the interfaces
        self.listNodes.update({num:getNodesFromGma("I{}".format(num),mesh,self.dofs['python'])})
        # Get the nodes of the local
        self.listNodes['A'] = getNodesFromGma("A{}".format(num),mesh,self.dofs["python"])
        # Convert to the dofs at the interfaces
        self.listDofs.update({0:getDofFromNodes(self.listNodes['A'],self.listNodes[num],len(self.dofs["python"]))})
        self.C.update({0:sp.coo_matrix((np.ones(len(self.listDofs[0])),(range(len(self.listDofs[0])),self.listDofs[0])),shape=(len(self.listDofs[0]),len(self.listNodes['A'])*len(self.dofs['python']))).tocsc()})
        self.t['A'] = sp.coo_matrix((np.ones(len(self.listDofs[0])),(range(len(self.listDofs[0])),self.listDofs[0])),shape=(len(self.listDofs[0]),len(self.listNodes['A'])*len(self.dofs['python']))).tocsc()

    