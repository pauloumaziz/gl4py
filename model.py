#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Functions to create aster models
"""

import code_aster


def threeDmeca():
    return {'aster':[code_aster.PhysicalQuantityComponent.Dx,code_aster.PhysicalQuantityComponent.Dy,code_aster.PhysicalQuantityComponent.Dz],
            'python':['DX','DY','DZ'],
            'asterForce':[code_aster.PhysicalQuantityComponent.Fx,code_aster.PhysicalQuantityComponent.Fy,code_aster.PhysicalQuantityComponent.Fz,code_aster.PhysicalQuantityComponent.Pres],
            'pythonForce':['FX','FY','FZ','PRES'],
            'physic':code_aster.Physics.Mechanics,
            'modeling':code_aster.Modelings.Tridimensional,
            'dimension':3}

def planeStrain():
    return {'aster':[code_aster.PhysicalQuantityComponent.Dx,code_aster.PhysicalQuantityComponent.Dy],
            'python':['DX','DY'],
            'asterForce':[code_aster.PhysicalQuantityComponent.Fx,code_aster.PhysicalQuantityComponent.Fy,code_aster.PhysicalQuantityComponent.Pres],
            'pythonForce':['FX','FY','PRES'],
            'physic':code_aster.Physics.Mechanics,
            'modeling':code_aster.Modelings.PlaneStrain,
            'dimension':2}

def planeStress():
    return {'aster':[code_aster.PhysicalQuantityComponent.Dx,code_aster.PhysicalQuantityComponent.Dy],
            'python':['DX','DY'],
            'asterForce':[code_aster.PhysicalQuantityComponent.Fx,code_aster.PhysicalQuantityComponent.Fy,code_aster.PhysicalQuantityComponent.Pres],
            'pythonForce':['FX','FY','PRES'],
            'physic':code_aster.Physics.Mechanics,
            'modeling':code_aster.Modelings.PlaneStress,
            'dimension':2}

def mechanical(modelisation):
    func = modelisationMechanicalSwitch.get(modelisation,None)
    return func()

modelisationMechanicalSwitch = {
        'planeStress' : planeStress,
        'planeStrain' : planeStrain,
        'tridimensional' : threeDmeca,
        }
phenomenonSwitch = {
        'mechanics' : mechanical
        }

def getDofsFromModelisation(phenomenon,modelisation):
    """
    Select the degrees of freedom associated to the phenomenon and the modeli-
    -sation.
    Input:
        -phenomenon: type of phenomenon (see doc aster of AFFE_MODELE)
        -modelisation: type of modelisation (see doc aster of AFFE_MODELE)
    Return a dictionnary with a list of aster dofs and a list of "python" dofs
    """
    func = phenomenonSwitch.get(phenomenon,None)
    return func(modelisation)

def createModel(mesh,physic,modeling,groupOfElements=None):
    """
    Create the aster model.
    Input: 
        -mesh : aster mesh
        -physic : string to describ the physic of the model
        -modeling : string to describ the modeling of the model
        -groupOfElements : list of string to precise the group of elements where to build the model
    Return the aster model
    """
    if not(physic in {'acoustics','mechanics','thermal'}):
        raise TypeError("The physic of the model doesn't correspond to an aster physics, {} must be in \n acoustics,mechanics,thermal".format(physic))
    dofs = getDofsFromModelisation(physic,modeling)
    model = code_aster.Model()
    model.setMesh(mesh)
    if groupOfElements:
        for group in groupOfElements:
            model.addModelingOnGroupOfElements(dofs['physic'],dofs['modeling'],group)
        # [model.addModelingOnGroupOfElements(dofs['physic'],dofs['modeling'],group) for group in groupOfElements]
    else:
        model.addModelingOnAllMesh(dofs['physic'],dofs['modeling'])
    model.build()
    return model,dofs
