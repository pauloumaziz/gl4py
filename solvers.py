#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Solvers 
"""

import numpy as np
from scipy import sparse as sp
import scipy.sparse.linalg as spl
import os

import code_aster
from code_aster.Commands import *

from gl4py.relaxation import aitken,aitkenMixed

import pdb
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

class globalLocal():
    """
    Global/local class
    """
    def __init__(self,itemax=20,tol=1e-5,lambG=None,path=None):
        """
        Initialization of the global/local solver
        """
        if not(path):
            path = os.getcwd()
        self.tol = tol
        self.iteMax = itemax
        self.err = 1.
        self.ite = 0
        self.lambG = lambG
        self.path = path

    def compute(self,patch,vcine,vneumann,patchAux=None):
        """
        Input:
            -patch: class patch
        """
        if rank == size -1:
            relax = aitken(patch.C['I'])
            lambG = self.lambG
        while self.err > self.tol and self.ite < self.iteMax:
            ## Global resolution
            # Extract the displacement on the interface
            if rank == size-1:
                uu = patch.operators['KLU'].solve(vcine - patch.operators['Kdiri'].dot(vcine) + lambG + vneumann )
                uu = relax.compute(uu)
                uGd = [patch.C[ii].dot(uu) for ii in range(patch.nLocal)]+[None]
            else:
                uGd = None
            uGd = comm.scatter(uGd,root=size-1)
            ## Local resolution
            if not(rank == size-1):
                ### Local and auxialiary resolutions
                # Solve the Dirichlet problem on the local domain
                uu = patch.operators['KLU'].solve(patch.C[0].T.dot(uGd) - patch.operators['Kdiri'].dot(patch.C[0].T.dot(uGd)) + vneumann)
                # Compute the nodal reaction on the interface for the local domain
                lambL = patch.C[0].dot(patch.operators['K'].dot(uu))
            else:
                lambL = None
                ### Compute the auxiliary reaction
                # uGaster = dict()
                lambA = dict()
                for ii in range(patch.nLocal):
                    uA = patch.t['A{}'.format(ii)].dot(uu)
                    lambA[ii] = patchAux[ii].C[0].dot(patchAux[ii].operators['K'].dot(uA))
                    # uGaster[ii] = convert2aster(patch.t['A{}'.format(ii)].dot(uG),patch.listNodes['A{}'.format(ii)],patch.dofs,mesh)
                    # lambA[ii] = computeNodalReactionOnElementsFromDisplacement(uGaster[ii],patch.model,patch.material,"I{}".format(ii),'A{}'.format(ii))
            lambL = comm.gather(lambL,root=size-1)
            if rank == size-1:
                lambL = lambL[:-1]
                oldLambG = lambG
                lambG = sum([patch.C[jj].T.dot(lambA[jj] - lambL[jj]) for jj in range(patch.nLocal)])
                rr = (lambG - oldLambG) 
                if np.linalg.norm(lambG) == 0:
                    self.err = np.linalg.norm(rr)**2
                else:
                    self.err = np.linalg.norm(rr)**2 / np.linalg.norm(lambG)**2
                with open(os.path.split(self.path)[0]+"/log.log","a") as log:
                    log.write("   Iteration {} ,  Error : {}\n".format(self.ite,self.err))
                with open(os.path.split(self.path)[0]+'/err.txt',"a") as errFile:
                    errFile.write("{} {}\n".format(self.ite,self.err))
            else:
                self.err = None
            self.err = comm.scatter([self.err for ii in range(patch.nLocal+1)],root=size-1)
            self.ite += 1
            print(self.err)
        return uu

class glatin():
    """
    Class for the global/local/latin solver
    """
    def __init__(self,itemax=100,tol=1e-5,tolFP=1e-5,u=None,w=dict(),f=dict(),wc=dict(),fc=dict(),path=None):
        """
        Initialization of the global/local solver
        """
        if not(path):
            path = os.getcwd()
        self.tol = tol
        self.tolFP = tolFP
        self.iteMax = itemax
        self.errW = 1.
        self.errF = 1.
        self.ite = 0
        self.u = u
        self.w = w
        self.wc = wc
        self.f = f
        self.fc = fc
        self.path = path

    def compute(self,patch,vcine,vneumann,patchAux=None,fixedPoint=False,u=None,w=None,wc=None,f=None,fc=None):
        """
        Compute the global/local/latin solver
        Input:
            -patch : class patch
            -vcine : numpy vector for the Dircihlet boundary conditions
            -vneumann : numpy vector for the Neumann boundary conditions
            -patchAux : auxiliar patches
            -fixedPoint : choice if fixed point for the linear stage
        """
        if not(u):
            uu = np.zeros(len(patch.listDofs['G']))
        if not(w):
            w = self.w
        if not(wc):
            wc = self.wc
        if not(f):
            f = self.f
        if not(fc):
            fc = self.fc
        wneighbor = dict()
        fneighbor = dict()
        for ii,neighbor in enumerate(patch.neighbor):
            [wneighbor[ii],fneighbor[ii]] = comm.sendrecv([w[ii],f[ii]],dest=neighbor,sendtag=rank,recvtag=neighbor) 
        ### Initialization of Aitken
        if rank == size -1:
            with open(os.path.split(self.path)[0]+"/log.log","a") as log:
                log.write("   Iteration  ,  Error :       W       -       F      \n -------------------------------------------------------\n")  
            lambA = {ii:np.zeros(len(patch.listDofs[ii])) for ii in patch.neighbor}
        relax = aitkenMixed([patch.C[ii] for ii in range(len(patch.neighbor))])
        uuOld = uu
        while (self.errF > self.tol  or self.errW > self.tol) and self.ite < self.iteMax:
            ### Local stage
            for ii,neighbor in enumerate(patch.neighbor):
                wc[ii] = 0.5 * (w[ii]+wneighbor[ii] - sp.linalg.spsolve(patch.operators['H'][ii],f[ii]+fneighbor[ii]))
                fc[ii] = f[ii] + patch.operators['H'][ii].dot(wc[ii] - w[ii])
            kWC = sum([patch.C[jj].T.dot(patch.operators['H'][jj].dot(wc[jj])) for jj,neighbor in enumerate(patch.neighbor)])
            FC = sum([patch.C[jj].T.dot(fc[jj]) for jj,neighbor in enumerate(patch.neighbor)])
            ### Linear stage
            lamb = FC + kWC
            ### Linear stage
            if rank == size-1:
                reacA = sum([patch.C[jj].T.dot(lambA[jj]) for jj,neighbor in enumerate(patch.neighbor)])
                uu = patch.operators['KLUlatin'].solve(vcine - patch.operators['Kdiri'].dot(vcine) + vneumann + lamb + reacA)
                ### Compute the auxiliary reaction
                for ii in range(patch.nLocal):
                    uA = patch.t['A{}'.format(ii)].dot(uu)
                    lambA[ii] = patchAux[ii].C[0].dot(patchAux[ii].operators['K'].dot(uA))
                    # uGaster[ii] = convert2aster(patch.t['A{}'.format(ii)].dot(uu),patch.listNodes['A{}'.format(ii)],patch.dofs,mesh)
                    # lambA[ii] = computeNodalReactionOnElementsFromDisplacement(uGaster[ii],patch.model,patch.material,"I{}".format(ii),'A{}'.format(ii))
            
                errFP = 1.
                iteFP = 1
                relaxFP = aitken(patch.C['I'])
                while errFP > 1e-8 and iteFP < 20 and fixedPoint:
                    reacA = sum([patch.C[jj].T.dot(lambA[jj]) for jj,neighbor in enumerate(patch.neighbor)])
                    uuOld = uu
                    uu = patch.operators['KLUlatin'].solve(vcine - patch.operators['Kdiri'].dot(vcine) + vneumann + lamb + reacA)
                    uu = relaxFP.compute(uu)
                    ### Compute the auxiliary reaction
                    for ii in range(patch.nLocal):
                        uA = patch.t['A{}'.format(ii)].dot(uu)
                        lambA[ii] = patchAux[ii].C[0].dot(patchAux[ii].operators['K'].dot(uA))
                        # uGaster[ii] = convert2aster(patch.t['A{}'.format(ii)].dot(uu),patch.listNodes['A{}'.format(ii)],patch.dofs,mesh)
                        # lambA[ii] = computeNodalReactionOnElementsFromDisplacement(uGaster[ii],patch.model,patch.material,"I{}".format(ii),'A{}'.format(ii))
                    r = uu - uuOld
                    if np.linalg.norm(uu) == 0:
                        errFP = np.linalg.norm(r)
                    else:
                        errFP = np.linalg.norm(r) / np.linalg.norm(uu)
                    iteFP +=1
                    with open(os.path.split(self.path)[0]+"/log.log","a") as log:
                        log.write("   error fixed point : {} \n".format(errFP))
                if fixedPoint:
                    with open(os.path.split(self.path)[0]+"/log.log","a") as log:
                        log.write("   Iterations fixed point : {} \n".format(iteFP))           
            else:
                uu = patch.operators['KLUlatin'].solve(lamb + vneumann + patch.operators['Kdiri'].dot(vcine))
            numWii = 0
            denWii = 0
            numFii = 0
            denFii = 0
            for ii,neighbor in enumerate(patch.neighbor):
                w[ii] = patch.C[ii].dot(uu) 
                f[ii] = fc[ii] + patch.operators['H'][ii].dot(wc[ii] - w[ii])
            ### Aitken relaxation
            kw,f = relax.compute([patch.operators['H'][ii].dot(w[ii]) for ii in range(len(patch.neighbor))],[f[ii] for ii in range(len(patch.neighbor))])
            w = {ii:sp.linalg.spsolve(patch.operators['H'][ii],kw[ii])  for ii,neighbor in enumerate(patch.neighbor)}
            uu = relax.w * uu + (1-relax.w) * uuOld
            for ii,neighbor in enumerate(patch.neighbor):
                [wneighbor[ii],fneighbor[ii]] = comm.sendrecv([w[ii],f[ii]],dest=neighbor,sendtag=rank,recvtag=neighbor)   
                ### Send interface fields
                numWii += (w[ii] - wneighbor[ii]).T.dot(w[ii] - wneighbor[ii])
                denWii += (w[ii] + wneighbor[ii]).T.dot(w[ii] + wneighbor[ii])
                numFii += (f[ii] + fneighbor[ii]).T.dot(f[ii] + fneighbor[ii])
                denFii += (f[ii] - fneighbor[ii]).T.dot(f[ii] - fneighbor[ii])
            if rank == size-1:
                if denWii == 0.:
                    self.errW = numWii
                else:
                    self.errW = numWii / denWii
                if denFii == 0.:
                    self.errF = numFii 
                else:
                    self.errF = numFii / denFii
            else:
                self.errW = None
                self.errF = None
            [self.errW,self.errF] = comm.scatter([[self.errW,self.errF] for ii in range(patch.nLocal+1)],root=size-1)
            self.ite += 1
            if rank==size-1:
                with open(os.path.split(self.path)[0]+"/log.log","a") as log:
                    log.write("Iteration : {} ,  Error : {} - {},  \n".format(self.ite,self.errW,self.errF))  
                with open(os.path.split(self.path)[0]+'/err.txt',"a") as errFile:
                    errFile.write("{} {} {}\n".format(self.ite,self.errW,self.errF))
        return uu

class glatinNoInt():
    """
    Class for the global/local/latin solver
    """
    def __init__(self,itemax=100,tol=1e-5,tolFP=1e-5,u=None,w=dict(),f=dict(),wc=dict(),fc=dict(),path=None):
        """
        Initialization of the global/local solver
        """
        if not(path):
            path = os.getcwd()
        self.tol = tol
        self.tolFP = tolFP
        self.iteMax = itemax
        self.errW = 1.
        self.errF = 1.
        self.ite = 0
        self.u = u
        self.w = w
        self.wc = wc
        self.f = f
        self.fc = fc
        self.path = path

    def compute(self,patch,vcine,vneumann,patchAux=None,fixedPoint=False,u=None,w=None,wc=None,f=None,fc=None):
        """
        Compute the global/local/latin solver
        Input:
            -patch : class patch
            -vcine : numpy vector for the Dircihlet boundary conditions
            -vneumann : numpy vector for the Neumann boundary conditions
            -patchAux : auxiliar patches
            -fixedPoint : choice if fixed point for the linear stage
        """
        if not(u):
            uu = np.zeros(len(patch.listDofs['G']))
        if not(w):
            w = self.w
        if not(wc):
            wc = self.wc
        if not(f):
            f = self.f
        if not(fc):
            fc = self.fc
        wneighbor = dict()
        fneighbor = dict()
        for ii,neighbor in enumerate(patch.neighbor):
            [wneighbor[ii],fneighbor[ii]] = comm.sendrecv([w[ii],f[ii]],dest=neighbor,sendtag=rank,recvtag=neighbor) 
        ### Initialization of Aitken
        if rank == size -1:
            with open(os.path.split(self.path)[0]+"/log.log","a") as log:
                log.write("   Iteration  ,  Error :       W       -       F      \n -------------------------------------------------------\n")  
            lambA = {ii:np.zeros(len(patch.listDofs[ii])) for ii in patch.neighbor}
        relax = aitkenMixed([patch.C[ii] for ii in range(len(patch.neighbor))])
        uuOld = uu
        while (self.errF > self.tol  or self.errW > self.tol) and self.ite < self.iteMax:
            ### Local stage
            for ii,neighbor in enumerate(patch.neighbor):
                wc[ii] = 0.5 * (w[ii]+wneighbor[ii] - sp.linalg.spsolve(patch.operators['H'][ii],f[ii]+fneighbor[ii]))
                fc[ii] = f[ii] + patch.operators['H'][ii].dot(wc[ii] - w[ii])
            kWC = sum([patch.C[jj].T.dot(patch.operators['H'][jj].dot(wc[jj])) for jj,neighbor in enumerate(patch.neighbor)])
            FC = sum([patch.C[jj].T.dot(fc[jj]) for jj,neighbor in enumerate(patch.neighbor)])
            ### Linear stage
            lamb = FC + kWC
            ### Linear stage
            if rank == size-1:
                reacA = sum([patch.C[jj].T.dot(lambA[jj]) for jj,neighbor in enumerate(patch.neighbor)])
                uu = patch.operators['KLU'].solve(vcine - patch.operators['Kdiri'].dot(vcine) + vneumann + lamb + reacA)
                ### Compute the auxiliary reaction
                for ii in range(patch.nLocal):
                    uA = patch.t['A{}'.format(ii)].dot(uu)
                    lambA[ii] = patchAux[ii].C[0].dot(patchAux[ii].operators['K'].dot(uA)) - patch.operators['H'][ii].dot(patch.C[ii].dot(uu))
                    # uGaster[ii] = convert2aster(patch.t['A{}'.format(ii)].dot(uu),patch.listNodes['A{}'.format(ii)],patch.dofs,mesh)
                    # lambA[ii] = computeNodalReactionOnElementsFromDisplacement(uGaster[ii],patch.model,patch.material,"I{}".format(ii),'A{}'.format(ii))
            
                errFP = 1.
                iteFP = 1
                relaxFP = aitken(patch.C['I'])
                while errFP > 1e-8 and iteFP < 20 and fixedPoint:
                    reacA = sum([patch.C[jj].T.dot(lambA[jj]) for jj,neighbor in enumerate(patch.neighbor)])
                    uuOld = uu
                    uu = patch.operators['KLU'].solve(vcine - patch.operators['Kdiri'].dot(vcine) + vneumann + lamb + reacA)
                    uu = relaxFP.compute(uu)
                    ### Compute the auxiliary reaction
                    for ii in range(patch.nLocal):
                        uA = patch.t['A{}'.format(ii)].dot(uu)
                        lambA[ii] = patchAux[ii].C[0].dot(patchAux[ii].operators['K'].dot(uA)) - patch.operators['H'][ii].dot(patch.C[ii].dot(uu))
                        # uGaster[ii] = convert2aster(patch.t['A{}'.format(ii)].dot(uu),patch.listNodes['A{}'.format(ii)],patch.dofs,mesh)
                        # lambA[ii] = computeNodalReactionOnElementsFromDisplacement(uGaster[ii],patch.model,patch.material,"I{}".format(ii),'A{}'.format(ii))
                    r = uu - uuOld
                    if np.linalg.norm(uu) == 0:
                        errFP = np.linalg.norm(r)
                    else:
                        errFP = np.linalg.norm(r) / np.linalg.norm(uu)
                    iteFP +=1
                    with open(os.path.split(self.path)[0]+"/log.log","a") as log:
                        log.write("   error fixed point : {} \n".format(errFP))
                if fixedPoint:
                    with open(os.path.split(self.path)[0]+"/log.log","a") as log:
                        log.write("   Iterations fixed point : {} \n".format(iteFP))           
            else:
                uu = patch.operators['KLUlatin'].solve(lamb + vneumann + patch.operators['Kdiri'].dot(vcine))
            numWii = 0
            denWii = 0
            numFii = 0
            denFii = 0
            for ii,neighbor in enumerate(patch.neighbor):
                w[ii] = patch.C[ii].dot(uu) 
                f[ii] = fc[ii] + patch.operators['H'][ii].dot(wc[ii] - w[ii])
            ### Aitken relaxation
            kw,f = relax.compute([patch.operators['H'][ii].dot(w[ii]) for ii in range(len(patch.neighbor))],[f[ii] for ii in range(len(patch.neighbor))])
            w = {ii:sp.linalg.spsolve(patch.operators['H'][ii],kw[ii])  for ii,neighbor in enumerate(patch.neighbor)}
            uu = relax.w * uu + (1-relax.w) * uuOld
            for ii,neighbor in enumerate(patch.neighbor):
                [wneighbor[ii],fneighbor[ii]] = comm.sendrecv([w[ii],f[ii]],dest=neighbor,sendtag=rank,recvtag=neighbor)   
                ### Send interface fields
                numWii += (w[ii] - wneighbor[ii]).T.dot(w[ii] - wneighbor[ii])
                denWii += (w[ii] + wneighbor[ii]).T.dot(w[ii] + wneighbor[ii])
                numFii += (f[ii] + fneighbor[ii]).T.dot(f[ii] + fneighbor[ii])
                denFii += (f[ii] - fneighbor[ii]).T.dot(f[ii] - fneighbor[ii])
            if rank == size-1:
                if denWii == 0.:
                    self.errW = numWii
                else:
                    self.errW = numWii / denWii
                if denFii == 0.:
                    self.errF = numFii 
                else:
                    self.errF = numFii / denFii
            else:
                self.errW = None
                self.errF = None
            [self.errW,self.errF] = comm.scatter([[self.errW,self.errF] for ii in range(patch.nLocal+1)],root=size-1)
            self.ite += 1
            if rank==size-1:
                with open(os.path.split(self.path)[0]+"/log.log","a") as log:
                    log.write("Iteration : {} ,  Error : {} - {},  \n".format(self.ite,self.errW,self.errF))  
                with open(os.path.split(self.path)[0]+'/err.txt',"a") as errFile:
                    errFile.write("{} {} {}\n".format(self.ite,self.errW,self.errF))
        return uu


class glatinNoIntGC():
    """
    Class for the global/local/latin solver
    """
    def __init__(self,itemax=100,tol=1e-5,tolFP=1e-5,u=None,w=dict(),f=dict(),wc=dict(),fc=dict(),path=None):
        """
        Initialization of the global/local solver
        """
        if not(path):
            path = os.getcwd()
        self.tol = tol
        self.tolFP = tolFP
        self.iteMax = itemax
        self.errW = 1.
        self.errF = 1.
        self.ite = 0
        self.u = u
        self.w = w
        self.wc = wc
        self.f = f
        self.fc = fc
        self.path = path

    def compute(self,patch,vcine,vneumann,patchAux=None,fixedPoint=False,u=None,w=None,wc=None,f=None,fc=None):
        """
        Compute the global/local/latin solver
        Input:
            -patch : class patch
            -vcine : numpy vector for the Dircihlet boundary conditions
            -vneumann : numpy vector for the Neumann boundary conditions
            -patchAux : auxiliar patches
            -fixedPoint : choice if fixed point for the linear stage
        """
        if not(u):
            uu = np.zeros(len(patch.listDofs['G']))
        if not(w):
            w = self.w
        if not(wc):
            wc = self.wc
        if not(f):
            f = self.f
        if not(fc):
            fc = self.fc
        wneighbor = dict()
        fneighbor = dict()
        for ii,neighbor in enumerate(patch.neighbor):
            [wneighbor[ii],fneighbor[ii]] = comm.sendrecv([w[ii],f[ii]],dest=neighbor,sendtag=rank,recvtag=neighbor) 
        ### Initialization of Aitken
        if rank == size -1:
            with open(os.path.split(self.path)[0]+"/log.log","a") as log:
                log.write("   Iteration  ,  Error :       W       -       F      \n -------------------------------------------------------\n")  
            lambA = {ii:np.zeros(len(patch.listDofs[ii])) for ii in patch.neighbor}
        relax = aitkenMixed([patch.C[ii] for ii in range(len(patch.neighbor))])
        uuOld = uu
        while (self.errF > self.tol  or self.errW > self.tol) and self.ite < self.iteMax:
            ### Local stage
            for ii,neighbor in enumerate(patch.neighbor):
                wc[ii] = 0.5 * (w[ii]+wneighbor[ii] - sp.linalg.spsolve(patch.operators['H'][ii],f[ii]+fneighbor[ii]))
                fc[ii] = f[ii] + patch.operators['H'][ii].dot(wc[ii] - w[ii])
            kWC = sum([patch.C[jj].T.dot(patch.operators['H'][jj].dot(wc[jj])) for jj,neighbor in enumerate(patch.neighbor)])
            FC = sum([patch.C[jj].T.dot(fc[jj]) for jj,neighbor in enumerate(patch.neighbor)])
            ### Linear stage
            lamb = FC + kWC
            ### Linear stage
            if rank == size-1:
                reacA = sum([patch.C[jj].T.dot(lambA[jj]) for jj,neighbor in enumerate(patch.neighbor)])
                bb =  vcine - patch.operators['Kdiri'].dot(vcine) + vneumann + lamb + reacA
                uu = gc([patch.operators['Kelim'],sum([patch.C[ii].T.dot(patch.operators['H'][ii].dot(patch.C[ii])) for ii in range(len(patch.neighbor))])],
                        bb,
                        uu,
                        patch.operators['KLU'])

                # uu = patch.operators['KLU'].solve(vcine - patch.operators['Kdiri'].dot(vcine) + vneumann + lamb + reacA)
                ### Compute the auxiliary reaction
                for ii in range(patch.nLocal):
                    uA = patch.t['A{}'.format(ii)].dot(uu)
                    lambA[ii] = patchAux[ii].C[0].dot(patchAux[ii].operators['K'].dot(uA))
                    # uGaster[ii] = convert2aster(patch.t['A{}'.format(ii)].dot(uu),patch.listNodes['A{}'.format(ii)],patch.dofs,mesh)
                    # lambA[ii] = computeNodalReactionOnElementsFromDisplacement(uGaster[ii],patch.model,patch.material,"I{}".format(ii),'A{}'.format(ii))
            else:
                # uu = patch.operators['KLUlatin'].solve(lamb + vneumann + patch.operators['Kdiri'].dot(vcine))
                bb = lamb + vneumann + vcine -  patch.operators['Kdiri'].dot(vcine)
                uu = gc([patch.operators['Kelim'],sum([patch.C[ii].T.dot(patch.operators['H'][ii].dot(patch.C[ii])) for ii in range(len(patch.neighbor))])],
                        bb,
                        uu,)
            numWii = 0
            denWii = 0
            numFii = 0
            denFii = 0
            for ii,neighbor in enumerate(patch.neighbor):
                w[ii] = patch.C[ii].dot(uu) 
                f[ii] = fc[ii] + patch.operators['H'][ii].dot(wc[ii] - w[ii])
            ### Aitken relaxation
            kw,f = relax.compute([patch.operators['H'][ii].dot(w[ii]) for ii in range(len(patch.neighbor))],[f[ii] for ii in range(len(patch.neighbor))])
            w = {ii:sp.linalg.spsolve(patch.operators['H'][ii],kw[ii])  for ii,neighbor in enumerate(patch.neighbor)}
            uu = relax.w * uu + (1-relax.w) * uuOld
            for ii,neighbor in enumerate(patch.neighbor):
                [wneighbor[ii],fneighbor[ii]] = comm.sendrecv([w[ii],f[ii]],dest=neighbor,sendtag=rank,recvtag=neighbor)   
                ### Send interface fields
                numWii += (w[ii] - wneighbor[ii]).T.dot(w[ii] - wneighbor[ii])
                denWii += (w[ii] + wneighbor[ii]).T.dot(w[ii] + wneighbor[ii])
                numFii += (f[ii] + fneighbor[ii]).T.dot(f[ii] + fneighbor[ii])
                denFii += (f[ii] - fneighbor[ii]).T.dot(f[ii] - fneighbor[ii])
            if rank == size-1:
                if denWii == 0.:
                    self.errW = numWii
                else:
                    self.errW = numWii / denWii
                if denFii == 0.:
                    self.errF = numFii 
                else:
                    self.errF = numFii / denFii
            else:
                self.errW = None
                self.errF = None
            [self.errW,self.errF] = comm.scatter([[self.errW,self.errF] for ii in range(patch.nLocal+1)],root=size-1)
            self.ite += 1
            if rank==size-1:
                with open(os.path.split(self.path)[0]+"/log.log","a") as log:
                    log.write("Iteration : {} ,  Error : {} - {},  \n".format(self.ite,self.errW,self.errF))  
                with open(os.path.split(self.path)[0]+'/err.txt',"a") as errFile:
                    errFile.write("{} {} {}\n".format(self.ite,self.errW,self.errF))
        return uu


class latin():
    """
    Class for the global/local/latin solver
    """
    def __init__(self,itemax=20,tol=1e-5,w=dict(),f=dict(),wc=dict(),fc=dict(),path=None):
        """
        Initialization of the global/local solver
        """
        if not(path):
            path = os.getcwd()
        self.tol = tol
        self.iteMax = itemax
        self.errW = 1.
        self.errF = 1.
        self.ite = 0
        self.w = w
        self.wc = wc
        self.f = f
        self.fc = fc
        self.path = path

    def compute(self,patch,vcine,vneumann,w=None,wc=None,f=None,fc=None):
        """
        Compute the global/local/latin solver
        Input:
            -patch : class patch
            -vcine : numpy vector for the Dirichlet boundary conditions
            -vneumann : numpy vector for the Neumann boundary conditions
            -patchAux : auxiliar patches
            -fixedPoint : choice if fixed point for the linear stage
        """
        if not(w):
            w = self.w
        if not(wc):
            wc = self.wc
        if not(f):
            f = self.f
        if not(fc):
            fc = self.fc
        wneighbor = dict()
        fneighbor = dict()
        for ii,neighbor in enumerate(patch.neighbor):
            [wneighbor[ii],fneighbor[ii]] = comm.sendrecv([w[ii],f[ii]],dest=neighbor,sendtag=rank,recvtag=neighbor) 
        ### Initialization of Aitken
        if rank == size -1:
            with open(os.path.split(self.path)[0]+"/log.log","a") as log:
                log.write("   Iteration  ,  Error :       W       -       F      \n -------------------------------------------------------\n")  
            # lambA = {ii:np.zeros(len(patch.listDofs[ii])) for ii in patch.neighbor}
        while (self.errF > self.tol  or self.errW > self.tol) and self.ite < self.iteMax:
            ### Local stage
            for ii,neighbor in enumerate(patch.neighbor):
                wc[ii] = 0.5 * (w[ii]+wneighbor[ii] - sp.linalg.spsolve(patch.operators['H'][ii],f[ii]+fneighbor[ii]))
                fc[ii] = f[ii] + patch.operators['H'][ii].dot(wc[ii] - w[ii])
                #fc[ii] = - 0.5 * (fneighbor[ii] - f[ii] - sp.linalg.spsolve(patch.operators['H'][ii],wneighbor[ii]-w[ii]))# * (-1)**(rank//(size-1))
            kWC = sum([patch.C[jj].T.dot(patch.operators['H'][jj].dot(wc[jj])) for jj,neighbor in enumerate(patch.neighbor)])
            FC = sum([patch.C[jj].T.dot(fc[jj]) for jj,neighbor in enumerate(patch.neighbor)])
            ### Linear stage
            lamb = FC + kWC
            ### Linear stage
            if rank == size-1:
                # reacA = sum([patch.C[jj].T.dot(lambA[jj]) for jj,neighbor in enumerate(patch.neighbor)])
                uu = patch.operators['KLUlatin'].solve(vcine - patch.operators['Kdiri'].dot(vcine) + vneumann + lamb)
            else:
                uu = patch.operators['KLUlatin'].solve(lamb + vneumann + patch.operators['Kdiri'].dot(vcine))
            numWii = 0
            denWii = 0
            numFii = 0
            denFii = 0
            for ii,neighbor in enumerate(patch.neighbor):
                w[ii] = patch.C[ii].dot(uu) 
                f[ii] = fc[ii] + patch.operators['H'][ii].dot(wc[ii] - w[ii])

                [wneighbor[ii],fneighbor[ii]] = comm.sendrecv([w[ii],f[ii]],dest=neighbor,sendtag=rank,recvtag=neighbor)   
                ### Send interface fields
                numWii += (w[ii] - wneighbor[ii]).T.dot(w[ii] - wneighbor[ii])
                denWii += (w[ii] + wneighbor[ii]).T.dot(w[ii] + wneighbor[ii])
                numFii += (f[ii] + fneighbor[ii]).T.dot(f[ii] + fneighbor[ii])
                denFii += (f[ii] - fneighbor[ii]).T.dot(f[ii] - fneighbor[ii])
            if rank == size-1:
                if denWii == 0.:
                    self.errW = numWii
                else:
                    self.errW = numWii / denWii
                if denFii == 0.:
                    self.errF = numFii 
                else:
                    self.errF = numFii / denFii
            else:
                self.errW = None
                self.errF = None
            [self.errW,self.errF] = comm.scatter([[self.errW,self.errF] for ii in range(patch.nLocal+1)],root=size-1)
            self.ite += 1
            if rank==size-1:
                with open(os.path.split(self.path)[0]+"/log.log","a") as log:
                    log.write("Iteration : {} ,  Error : {} - {},  \n".format(self.ite,self.errW,self.errF))  
                with open(os.path.split(self.path)[0]+'/err.txt',"a") as errFile:
                    errFile.write("{} {} {}\n".format(self.ite,self.errW,self.errF))
        return uu

def gc(A,bb,xx,M=None,tol=1e-7,iteMax=20):
    """
    Preconditioned conjugate gradient algorithm
    Input:
        -A: list of contributions to final operator A
        -b: right hand side
        -M: preconditioner
        -tol: tolerance
    """
    if not(M):
        M = sp.linalg.splu(sp.eye(len(bb),len(bb)))
    ite = 0
    Ax = sum([Ai.dot(xx) for Ai in A])
    rr = bb - Ax
    zz = M.solve(rr)
    pp = zz    
    AA = A[0] + A[1]
    def Amul(vv):
        return A[0].dot(vv) + A[1].dot(vv)
    def Minv(vv):
        return M.solve(vv)
    AAA = spl.LinearOperator(A[0].shape,matvec=Amul)
    MM = spl.LinearOperator(A[0].shape,matvec=Minv)
    xx = spl.cg(AAA,bb,xx,tol=1e-7,M=MM)[0]
    # while np.linalg.norm(rr) > tol and ite<iteMax:
    #     rrOld = rr
    #     zzOld = zz
    #     ppOld = pp
    #     Ap = sum([Ai.dot(pp) for Ai in A])
    #     aj = rr.T.dot(zz) / (pp.T.dot(Ap))
    #     xx += aj * pp
    #     rr = rr -  aj * Ap
    #     zz = M.solve(rr)
    #     # bj = rr.T.dot(zz)/(rrOld.T.dot(zzOld))
    #     bj =  - zz.dot(Ap) / (pp.T.dot(Ap))
    #     pp = zz + bj * ppOld
    #     ite += 1
    #     print(np.linalg.norm(rr))
    #     pdb.set_trace()
    return xx

