#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Functions to compute the relaxation
"""

import numpy as np
import pdb
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

class aitken():
    """
    Class to compute the dynamic relaxation Aitken delta square
    """
    def __init__(self,C):
        """
        Init function  for the aitken class
        """
        self.iteration = 0
        self.w = 1
        self.C = C
        sizeInterfaceVector = C.shape[0]
        self.uu = np.zeros(C.shape[1])
        self.delta = [np.zeros(sizeInterfaceVector),np.zeros(sizeInterfaceVector),np.zeros(sizeInterfaceVector)]

    def compute(self,uu):
        """
        Compute the Aitken relaxation
        Input:
            -ite: iteration of the algorithm
            -uu : displacement field
            -uuOld : displacement field before the iteration
            -C : operator to extract the trace on the interface
        """
        if self.iteration > 1:
            self.delta = [self.delta[1],self.delta[2],self.C.dot(uu - self.uu)]
            self.w = -self.w * self.delta[1].T.dot(self.delta[2]-self.delta[1]) / np.linalg.norm(self.delta[2] - self.delta[1])**2
            uuNew = self.w * uu + (1-self.w) * self.uu
        else:
            uuNew = uu
            self.delta = [self.delta[1],self.delta[2],self.C.dot(uu - self.uu)]
        self.uu = uuNew
        self.iteration += 1
        return uuNew


# class aitkenFalse():
#     """
#     Class to compute the dynamic relaxation Aitken delta square
#     """
#     def __init__(self,C):
#         """
#         Init function  for the aitken class
#         """
#         self.iteration = 0
#         self.w = 1
#         self.C = C
#         sizeInterfaceVector = C.shape[0]
#         self.uu = np.zeros(C.shape[1])
#         self.delta = [np.zeros(sizeInterfaceVector),np.zeros(sizeInterfaceVector),np.zeros(sizeInterfaceVector)]

#     def compute(self,uu):
#         """
#         Compute the Aitken relaxation
#         Input:
#             -ite: iteration of the algorithm
#             -uu : displacement field
#             -uuOld : displacement field before the iteration
#             -C : operator to extract the trace on the interface
#         """
#         if self.iteration > 1:
#             uuNew = self.w * uu + (1-self.w) * self.uu
#             self.delta = [self.delta[1],self.delta[2],self.C.dot(uu - self.uu)] 
#             self.w = -self.w * self.delta[1].T.dot(self.delta[2]-self.delta[1]) / np.linalg.norm(self.delta[2] - self.delta[1]) ### Attention n'y aurait-il pas un carré d'oublié ???
#             pdb.set_trace()
#         else:
#             uuNew = uu
#             self.delta = [self.delta[1],self.delta[2],self.C.dot(uu - self.uu)]
#         self.uu = uuNew
#         self.iteration += 1
#         return uuNew


class aitkenMixed():
    """
    Class to compute the dynamic relaxation Aitken delta square
    """
    def __init__(self,C):
        """
        Init function  for the aitken class
        Input:
            -C : list of trace operator on the interfaces
        """
        self.iteration = 0
        self.w = 1
        self.C = C
        self.sizeInterfaceVector = [Cii.shape[0] for Cii in C]
        self.uu = np.zeros(2*sum(self.sizeInterfaceVector))
        # self.ww = [np.zeros(sizeVector) for sizeVector in self.sizeInterfaceVector]
        # self.ff = [np.zeros(sizeVector) for sizeVector in self.sizeInterfaceVector]
        self.delta = [np.zeros(2*sum(self.sizeInterfaceVector)),np.zeros(2*sum(self.sizeInterfaceVector))]

    def compute(self,ww,ff):
        """
        Compute the Aitken relaxation
        Input:
            -ww : list of field
            -ff : list of field
        """
        uu = np.hstack((np.hstack(ww),np.hstack(ff)))
        if self.iteration > 1:
            self.delta = [self.delta[1],uu-self.uu] 
            num = self.delta[0].T.dot(self.delta[1]-self.delta[0])
            den = (self.delta[1] - self.delta[0]).T.dot(self.delta[1] - self.delta[0])
            res = comm.allreduce(np.array([num,den]),op=MPI.SUM)
            self.w = -self.w * res[0] / res[1]
            uuNew = self.w * uu + (1-self.w) * self.uu
        else:
            uuNew = uu
            self.delta = [self.delta[1],uu-self.uu]
        self.uu = uuNew
        self.iteration += 1
        wwNew = {ii:self.uu[ii*sizeVector:(ii+1)*sizeVector] for ii,sizeVector in enumerate(self.sizeInterfaceVector)}
        ffNew = {ii:self.uu[sum(self.sizeInterfaceVector)+ii*sizeVector:sum(self.sizeInterfaceVector)+(ii+1)*sizeVector] for ii,sizeVector in enumerate(self.sizeInterfaceVector)}
        return wwNew,ffNew



