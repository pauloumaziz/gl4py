#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Functions to manage boundary conditions
"""
import pdb
import code_aster
from code_aster.Commands import CALC_VECT_ELEM,ASSE_VECTEUR,CREA_CHAMP



def computeNeumann(mod,mat,numDofs,neumannBC,dofs):
    """
    Functions to compute the right-hand-side for Neumann boundary conditions
    Input:
        -mod: aster model
        -mat: aster material
        -neumannBC: dictionnary of Neumann boundary condition
        -dofs: dictionnary containing the aster and python dofs
    """
    neumann_conditions = list()
    neumannVectorList = list()
    for ii,typeNeumann in enumerate(neumannBC):
        neumann_conditions.append(neumann.get(typeNeumann)(mod,mat,neumannBC[typeNeumann],dofs))
    VEF = CALC_VECT_ELEM(OPTION ='CHAR_MECA',
                         CHARGE = neumann_conditions,
                         CHAM_MATER = mat,
                            )
    Fd_asse = ASSE_VECTEUR(VECT_ELEM = VEF,
                            NUME_DDL = numDofs,
                            )
    F_asse = CREA_CHAMP(OPERATION = 'ASSE',
                        TYPE_CHAM = 'NOEU_DEPL_R',
                        MODELE = mod,
                        NUME_DDL = numDofs,
                        PROL_ZERO = 'OUI',
                        ASSE = _F(TOUT='OUI', CHAM_GD = Fd_asse, CUMUL='OUI'),
                        )
        # neumannVectorList.append(F_asse.EXTR_COMP().valeurs)
    neumannVector = F_asse.EXTR_COMP().valeurs
    return F_asse,neumannVector

def lineicForce(mod,mat,neumannBC,dofs):
    """
    Build the right-hand-side for a lineic force
    Input:
        -mod: aster model
        -mat: aster material
        -neumannBC: dictionnary of Neumann boundary condition
        -dofs: dictionnary containing the aster and python dofs
    """

    boundaryNeumann = code_aster.LineicForceDouble(mod)
    for bc,gma in enumerate(neumannBC):
        imposedBC = code_aster.ForceDouble()
        imposedBC.setValue(code_aster.PhysicalQuantityComponent.Fx,neumannBC[gma]['FX'])
        imposedBC.setValue(code_aster.PhysicalQuantityComponent.Fy,neumannBC[gma]['FY'])
        boundaryNeumann.setValue(imposedBC,gma)
    boundaryNeumann.build()
    return boundaryNeumann


def imposedDOFonGNO(mod,valueBC,dofs,multiplier=False):
    """
    Return a kinematics boundary condition instance
    Input:
        -mod: aster model of the structure
        -valueBC: dictionnary of nodes groups including value and component of the boundary conditions
        -dofs: dictionnary containing the aster and python dofs
    Output:
        aster boundary condition of type char_cine
    """

    dof_connection = {dofPython:dofs['aster'][dofs['python'].index(dofPython)] for dofPython in dofs['python']}
    if not multiplier:
        imposedDof = code_aster.KinematicsMechanicalLoad()
        imposedDof.setModel(mod)
        for ii,gno in enumerate(valueBC):
            for dof in list(valueBC[gno].keys()):
                imposedDof.addImposedMechanicalDOFOnNodes(dof_connection[dof],valueBC[gno][dof],gno)
        imposedDof.build()

    else:
        imposedDof = code_aster.ImposedDisplacementDouble(mod)
        for ii,gno in enumerate(valueBC):
            imposedDofii = code_aster.DisplacementDouble()
            for dof in list(valueBC[ii].keys()):
                imposedDofii.setValue(dof_connection[dof],valueBC[ii][dof])
            imposedDof.setValue(imposedDofii,gno)
        imposedDof.build()

    return imposedDof

def surfaceForce(mod,mat,neumannBC,dofs):
    """
    Build the right-hand-side for a lineic force
    Input:
        -mod: aster model
        -mat: aster material
        -neumannBC: dictionnary of Neumann boundary condition
        -dofs: dictionnary containing the aster and python dofs
    """
    boundaryNeumann = code_aster.ForceOnFaceDouble(mod)
    for bc,gma in enumerate(neumannBC):
        imposedBC = code_aster.ForceDouble()
        imposedBC.setValue(code_aster.PhysicalQuantityComponent.Fx,neumannBC[gma]['FX'])
        imposedBC.setValue(code_aster.PhysicalQuantityComponent.Fy,neumannBC[gma]['FY'])
        imposedBC.setValue(code_aster.PhysicalQuantityComponent.Fz,neumannBC[gma]['FZ'])
        boundaryNeumann.setValue(imposedBC,gma)
    boundaryNeumann.build()
    return boundaryNeumann

neumann = {'lineic':lineicForce,
           'surface':surfaceForce,
        #'weight':distributeForce,
        }