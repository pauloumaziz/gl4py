#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Functions to manage stiffness
"""
import numpy as np
import scipy.sparse as sp

import code_aster
from code_aster.Commands import CALC_CHAR_CINE

def computeRHSfromAsterDiri(diri,numDof):
    """
    Compute the numpy array of a Dirichlet conditions with a given numbering
    Input:
        -diri: kinematics aster boundary conditions
        -numDof: aster numberinf of dofs
    """
    rhs = CALC_CHAR_CINE(NUME_DDL=numDof,CHAR_CINE=diri)
    return rhs.EXTR_COMP().valeurs

def computeStiffness(mod,mat,dirichlet=None):
    """
    Compute the stiffness operator.
    Input:
        -mod: aster model
        -mat: assigned aster material
        -dirichlet: potential dirchlet boundary conditions
    Return a stiffness aster concept
    """
    ### Create the study
    study = code_aster.StudyDescription(mod, mat)
    ### Create the discrete problem
    discreteProblem = code_aster.DiscreteProblem(study)
    ### Create the elementary matrices
    matr_elem = discreteProblem.computeMechanicalStiffnessMatrix()
    ### Compute the numbering of the dofs
    numDof = code_aster.DOFNumbering()
    numDof.setElementaryMatrix( matr_elem )
    numDof.computeNumbering()
    ### Assemble the stiffness matrix
    matrAsse = code_aster.AssemblyMatrixDisplacementDouble()
    matrAsse.appendElementaryMatrix( matr_elem )
    matrAsse.setDOFNumbering( numDof )
    if dirichlet:
        matrAsse.addKinematicsLoad(dirichlet)
    matrAsse.build()
    if dirichlet:
        ccid = matrAsse.sdj.cine.CCID.get()
        dofElim = (np.array(ccid[:-1],dtype=bool),ccid[-1])
    else:
        dofElim = (0)

    return matrAsse,numDof,dofElim

def convertAsterStiffnessToPython(stiffness,sparse=True):
    """
    Convert the aster stiffness in a dense numpy array or a sparse scipy matrix
    """
    stiffnessPython = stiffness.EXTR_MATR(sparse=sparse)
    if sparse:
        stiffnessPython = sp.coo_matrix((stiffnessPython[0],
                                        (stiffnessPython[1],stiffnessPython[2])),
                                        (stiffnessPython[3],stiffnessPython[3])).tocsc()
    return stiffnessPython

def elimOperatorOnNodes(stiffness,dofElim):
    """
    Compute the part of the eliminated part of the operator for the case
    of non zeros dirichlet boundary conditions
    Input:
        -stiffness: python operator
        -dofElim: (numpy array of the eliminated dof,number of eliminated dof) 
    """
    
    stiffness_extr = sp.csc_matrix(stiffness.shape)
    if not(dofElim[-1]==0):
        stiff_inter = stiffness[:,dofElim[0]].tocsr()
        stiff_inter[dofElim[0],:] = 0.*sp.eye(dofElim[1],dofElim[1]).tocsr()
        stiffness_extr[:,dofElim[0]] = stiff_inter.tocsc()
    return stiffness_extr

def modifSparseMatrixOnNodes(mat1,listMat,listNodes,listNodesGlobal,op='add'):
    """
    Modify a matrix by another ones on the dofs associated to a list of nodes.
    Input:
        -mat1: matrix to modify
        -listMat: matrix used for the modification
        -listNodes: list of nodes where to modify
        -listNodesTot: list of nodes of the 1st matrix
        -op: type of the operation, by default it is a sum
    """
    dim = mat1.shape[0] // len(listNodesGlobal)
    dict_nodes = {node:pos for pos,node in enumerate(listNodesGlobal)}

    if not(isinstance(listMat,list)):
        listMat = [listMat]
    if not(isinstance(listNodes[0],list)):
        listNodes = [listNodes]
    for kk,mat in enumerate(listMat):
        pos_in_nodes = [dict_nodes[node] for node in listNodes[kk]]
        pos_in_valField = [dim * ii+jj for ii in pos_in_nodes for jj in range(dim)]
        if op=="add":
            mat1[np.ix_(pos_in_valField,pos_in_valField)] = mat1[np.ix_(pos_in_valField,pos_in_valField)] + mat