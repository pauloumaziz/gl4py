#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Functions to create aster material
"""

import code_aster
from code_aster.Commands import DEFI_MATERIAU

# =============================================================================
# Builder for isotropic material
# =============================================================================
def isotropicBuilder(matDict,weight=1.):
    """
    Build the aster material
    Input:
        -matDict: dictionnary which describes the material
        -weight: potential weighting of mechanical caracteristics
    Output:
        - a concept material aster
    """
    ### Get the material properties
    young  = weight * matDict['E']
    poisson = matDict['nu']
    rho = matDict['rho']
    ### Create the aster concept
#    mater = code_aster.ElasMaterialBehaviour()
#    mater.setDoubleValue("E",young)
#    mater.setDoubleValue("Nu",poisson)
#    mater.setDoubleValue("Rho",rho)
#
#    mat = code_aster.Material()
#    mat.addMaterialBehaviour(mater)
#    mat.build()
    mat = DEFI_MATERIAU(ELAS=_F(E = young,NU=poisson,RHO=rho))

    return mat


materialBuilder = {
        'isotropic' : isotropicBuilder}

def assignMaterialDict(mat_dict,meshAster):
    """
    Return an aster material instance
    Input:
        -mat_dict: dictionnary for which keys are the name of groups of
        elements and the aster material
        -meshAster: mesh aster
    Output:
        - aster material assigned on a mesh
    """
    affectMat = code_aster.MaterialOnMesh(meshAster)
    for group in list(mat_dict.keys()):
        affectMat.addMaterialOnGroupOfElements(mat_dict[group],[group])
    affectMat.buildWithoutInputVariables()
    return affectMat

def createAsterMaterial(matDict,weight=1.):
    """
    Create an aster material.
    Input:
        -matDict: dictionnary which describes the material
        -weight: potential weighting of mechanical caracteristics
    Output:
        - a concept material aster
    """
    mat = materialBuilder.get(matDict['type'],None)(matDict,weight)
    return mat